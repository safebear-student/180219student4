package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test
    public void LoginTest() {

        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The Login Page didn't open, or the title has changed");
        loginPage.enterUserName("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        Assert.assertEquals(toolsPage.getpageTitle(), "Tools Page", "The tools Page didn't open, or the title text changed");
    }

}
